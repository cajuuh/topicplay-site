using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
public class UsersDAO
{
    
    public ObjectId _id { get; set; }
    [StringLength(100, MinimumLength = 2, ErrorMessage = "The Name field requires a minimum of 2 and a maximum of 100 characters<br />")]
    [Required(ErrorMessage = "The Name field is required<br />")]
    [BsonElement("Name")]
    public string Name { get; set; }

    [EmailAddress(ErrorMessage = "The email field is in an incorrect format<br />")]
    [Required(ErrorMessage = "The Email field is required<br />")]
    [BsonElement("Email")]
    public string Email { get; set; }

    [StringLength(100, MinimumLength = 8, ErrorMessage = "The Password field requires a minimum of 8 and a maximum of 100 characters<br />")]
    [Required(ErrorMessage = "The Password field is required<br />")]
    [BsonElement("Password")]
    public string Password { get; set; }

    [BsonElement("Group")]
    public int Group { get; set; }

    [BsonElement("Acls")]
    public ICollection<int> Acls { get; set; }

    [BsonElement("Status")]
    public string Status { get; set; }
}
