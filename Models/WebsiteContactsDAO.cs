using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

public class WebsiteContactsDAO
{
    //public ObjectId _Id { get; set; }

    /*[BsonElement("Id")]
    [Required(ErrorMessage = "O campo nome é Obrigatório<br />")]
    public int Id { get; set; }
    */

    [StringLength(100, MinimumLength = 2, ErrorMessage = "The Name field requires a minimum of 2 and a maximum of 100 characters<br />")]
    [Required(ErrorMessage = "The Name field is required<br />")]
    [BsonElement("Name")]
    public string Name { get; set; }

    [EmailAddress(ErrorMessage = "The email field is in an incorrect format<br />")]
    [Required(ErrorMessage = "The Email field is required<br />")]
    [BsonElement("Email")]
    public string Email { get; set; }

    [Required(ErrorMessage = "The Message field is required<br />")]
    [BsonElement("Message")]
    public string Message { get; set; }

    [BsonElement("DateTime")]
    public string DateTime { get; set; }

    [BsonElement("Status")]
    public int Status { get; set; }
}
