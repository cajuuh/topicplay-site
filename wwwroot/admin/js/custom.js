/* ---------------------------------------------
 Contact form
 --------------------------------------------- */
 $(document).ready(function(){
    
        $("#login_form").submit(function(){
            
        var str = $(this).serialize();
            
            $.ajax({
                type:"POST",
                url:"/Admin/DoLogin",
                dataType: 'json',
                data: str,
                beforeSend: function()
                {
                    $("#result").hide().html('<div class="alert alert-warning">Loading...</div>').slideDown();
                },
                success: function(response){
                    //load json data from server and output message     
                    if (response.error == true) 
                    {
                        output = '<div class="alert alert-danger">' + response.message +  '</div>';
                    }
                    else 
                    {
                        output = '<div class="alert alert-success">' + response.message + '</div>';
                        $("#login_form")[0].reset();
                        document.location = '/Admin/Dashboard';
                        
                    }
                    
                    $("#result").hide().html(output).slideDown();
                }
            }); //fecha o ajax.
            return false;
        });
        
       
        
    });
    