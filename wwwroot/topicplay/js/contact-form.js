/* ---------------------------------------------
 Contact form
 --------------------------------------------- */
$(document).ready(function(){

    $("#contact_form").submit(function(){
        
    var str = $(this).serialize();
        
        $.ajax({
            type:"POST",
            url:"/Home/SaveContact",
            dataType: 'json',
            data: str,
            beforeSend: function()
            {
                $("#result").hide().html('<div class="notice">Loading</div>').slideDown();
            },
            success: function(response){
                //load json data from server and output message     
                if (response.error == true) 
                {
                    output = '<div class="error">' + response.message +  '</div>';
                }
                else 
                {
                    output = '<div class="success">' + response.message + '</div>';
                    $("#contact_form")[0].reset();
                }
                
                $("#result").hide().html(output).slideDown();
            }
        }); //fecha o ajax.
        return false;
    });
    
    
});
