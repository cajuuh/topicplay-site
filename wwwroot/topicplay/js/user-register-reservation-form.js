/* ---------------------------------------------
 Contact reservations form
 --------------------------------------------- */
 $(document).ready(function(){
    $("#register_btn").click(function(){
        
        //get input field values
        var user_name = $('input[name=name]').val();
        var user_email = $('input[name=email]').val();
        var user_password = $('input[name=password]').val();
        var user_rePassword = $('input[name=repassword]').val();
        
        //simple validation at client's end
        //we simply change border color to red if empty field using .css()
        var proceed = true;
        if (user_name == "") {
            $('input[name=name]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_email == "") {
            $('input[name=email]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_password == "") {
            $('input[name=password]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_password != user_rePassword) {
            $('input[name=repassword]').css('border-color', '#e41919');
            proceed = false;
        }
        
        //everything looks good! proceed...
        if (proceed) {
            //data to be sent to server
            post_data = {
                'userName': user_name,
                'userEmail': user_email,
                'userPassword': user_password,
            };
            
            //Ajax post data to server
            $.post('user_register.php', post_data, function(response){
            
                //load json data from server and output message     
                if (response.type == 'error') {
                    output = '<div class="error">' + response.text + '</div>';
                }
                else {
                
                    output = '<div class="success">' + response.text + '</div>';
                    
                    //reset values in all input fields
                    $('#signin_form input').val('');
                }
                
                $("#result").hide().html(output).slideDown();
            }, 'json');
            
        }
        
        return false;
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#signin_form input").keyup(function(){
        $("#signin_form input").css('border-color', '');
        $("#result").slideUp();
    });
    
});
