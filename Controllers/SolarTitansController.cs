﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using topicplay_solartitans.Repository;

namespace topicplay_solartitans.Controllers
{
    public class SolarTitansController : Controller
    {
        protected UsersREP _UsersREP;
        public SolarTitansController()
        {
            _UsersREP = new UsersREP();
        }
        public IActionResult Index()
        {
            ViewData["Title"] = "SolarTitans";
            return View();
        }

        [HttpPost]
        public IActionResult SaveUser(UsersDAO users)
        {
            if (ModelState.IsValid)
            {
                // users.DateTime = DateTime.Now.ToString();
                users.Status = "registered";
                users.Group = 1;
                _UsersREP.Insert(users);
                return Json(new {error = false, message= "You've as been successfully registered, download SolarTitans and login."});
            }
            else
            {
                var messageArray = this.ViewData.ModelState.Values.SelectMany(modelState => modelState.Errors, (modelState, error) => error.ErrorMessage).ToArray();
                return Json(new {error = true, message = messageArray});
            }
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
