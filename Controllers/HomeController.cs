﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using topicplay_solartitans.Repository;

namespace topicplay_solartitans.Controllers
{
    public class HomeController : Controller
    {
        protected WebsiteContactsREP _WebsiteContactsREP;
        public HomeController()
        {
            _WebsiteContactsREP = new WebsiteContactsREP();
        }
        public IActionResult Index()
        {
            ViewData["Title"] = "Topic Play Games";
            return View();
        }

      
        [HttpPost]
        public IActionResult SaveContact(WebsiteContactsDAO contacts)
        {
            if (ModelState.IsValid)
            {
                contacts.DateTime = DateTime.Now.ToString();
                contacts.Status = 2;
                _WebsiteContactsREP.Insert(contacts);
                return Json(new {error = false, message= "Your message was successfully sent, we will reply shortly."});
            }
            else
            {
                var messageArray = this.ViewData.ModelState.Values.SelectMany(modelState => modelState.Errors, (modelState, error) => error.ErrorMessage).ToArray();
                return Json(new {error = true, message = messageArray});
            }
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}
