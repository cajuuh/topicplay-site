using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using topicplay_solartitans.Repository;

namespace topicplay_solartitans.Controllers
{
    public class AdminController : Controller
    {
        protected WebsiteContactsREP _WebsiteContactsREP;
        protected UsersREP _UsersREP;

        public AdminController()
        {
            _WebsiteContactsREP = new WebsiteContactsREP();
            _UsersREP = new UsersREP();
        }
        public IActionResult Index()
        {
            ViewData["Title"] = "Login - Topic Play Games";
            return View();
        }

        public IActionResult Dashboard()
        {
            
            if(HttpContext.Session.GetString("SessionKeyEmail") != null && HttpContext.Session.GetInt32("SessionStatus") == 1)
            {
                ViewData["Title"] = "Dashboard - Topic Play Games";
                return View();
            }
            else
            {
                return DoLogout();
            }
            
        }

        private static string getHash(string text) {
            // SHA512 is disposable by inheritance.
            using (var sha256 = SHA256.Create()) {
                // Send a sample text to hash.
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));
        
                // Get the hashed string.
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

        [HttpPost]
        public IActionResult DoLogin(UsersDAO user)
        {
            if(user.Email == "" || user.Password == "")
            {
                 return Json(new {error = true, message = "All the fields are required!"});
            }
            else
            {
                if(_UsersREP.VerifyUserLogin(user.Email,getHash(user.Password)) == null)
                {
                    return Json(new {error = true, message = "User or Password not found"});
                }
                else
                {
                    HttpContext.Session.SetString("SessionKeyEmail", user.Email);
                    HttpContext.Session.SetInt32("SessionStatus", 1);
                    return Json(new {error = false, message = "You are successfully logged in!"});
                }
                //Console.WriteLine(test);
            }
        }
        
        [HttpGet]
        public IActionResult DoLogout()
        {
            //HttpContext.Session.Remove("SessionKeyEmail");
            //HttpContext.Session.Remove("SessionStatus");
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        public IActionResult ListWebsiteContacts()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}
