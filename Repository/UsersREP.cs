
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using MongoDB.Bson.Serialization;

namespace topicplay_solartitans.Repository
{
    /*
    mongod --dbpath ~/data/db --auth
    mongod
    mongo
    dotnet restore
    dotnet run
     */
     
    public class UsersREP
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected IMongoCollection<UsersDAO> _collection;

        public UsersREP()
        {
            _client = new MongoClient();
            _database = _client.GetDatabase("topicplay");
            _collection = _database.GetCollection<UsersDAO>("Users");
        }

        public UsersDAO Insert(UsersDAO user)
        {
            this._collection.InsertOneAsync(user);
            //return this.Get(contact._id.ToString());
            return user;
        }

        /*public List<UsersDAO> Filter(string jsonQuery)
        {
            var queryDoc = BsonSerializer.Deserialize<BsonDocument>(jsonQuery);
            return _collection.Find<UsersDAO>(queryDoc).ToList();
        }*/


        
        public UsersDAO VerifyUserLogin(string email, string pass)
        {
            return this._collection.Find(new BsonDocument { { "Email", email }, { "Password", pass } }).SingleOrDefault();
        }

    }
}