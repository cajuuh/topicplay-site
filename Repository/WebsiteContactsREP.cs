
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using MongoDB.Bson.Serialization;

namespace topicplay_solartitans.Repository
{
    /*
    mongod --dbpath ~/data/db --auth
    mongod
    mongo
    dotnet restore
    dotnet run
     */
     
    public class WebsiteContactsREP
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        protected IMongoCollection<WebsiteContactsDAO> _collection;

        public WebsiteContactsREP()
        {
            _client = new MongoClient();
            _database = _client.GetDatabase("topicplay");
            _collection = _database.GetCollection<WebsiteContactsDAO>("WebsiteContacts");
        }

        public WebsiteContactsDAO Insert(WebsiteContactsDAO contact)
        {
            this._collection.InsertOneAsync(contact);
            //return this.Get(contact._id.ToString());
            return contact;
        }

        public WebsiteContactsDAO Get(string id)
        {
            return this._collection.Find(new BsonDocument { { "_id", new ObjectId(id) } }).FirstAsync().Result;
        }

    }
}